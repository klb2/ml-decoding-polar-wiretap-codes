from datetime import datetime
from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
from digcommpy import channels, decoders, encoders, messages, modulators

def ber_vs_snr_curve(code_length, snr_bob, snr_eve, nn_options,
                     svm_options, test_snr=range(-10, 6), test_size=1e6,
                     metric=['ber', 'bler']):
    # Encoder and Modulator
    modulator = modulators.BpskModulator()
    encoder = encoders.PolarWiretapEncoder(code_length, "BAWGN", "BAWGN",
                                           snr_bob, snr_eve)
    info_length = encoder.info_length
    info_length_bob = encoder.info_length_bob
    # Channel
    channel = channels.BawgnChannel(snr_bob, rate=info_length_bob/code_length,
                                    input_power=1.)
    # Decoders
    dec_ref = decoders.PolarWiretapDecoder(code_length, "BAWGN", snr_bob,
                                           pos_lookup=encoder.pos_lookup)
    dec_nn = decoders.NeuralNetDecoder(code_length, info_length, **nn_options)
    dec_svm = decoders.SvmDecoder(code_length, info_length, **svm_options)
    ### Train the ML decoders
    print("N={}, k={}, k_bob={}".format(code_length, info_length, info_length_bob))
    print("Started training the NN decoder")
    dec_nn.train_system((encoder, modulator), epochs=10000)
    print("Started training the SVM decoder")
    dec_svm.train_system((encoder, modulator))
    _decoders = {"nn": dec_nn, "svm": dec_svm, "ref": dec_ref}
    ## Test the trained decoders
    print("Finished training. Started testing...")
    errors = {n: {s: {k: 0 for k in metric} for s in test_snr}
              for n in _decoders.keys()}
    test_size = int(test_size)
    tx_messages_gen = messages._generate_data_generator(info_length=info_length,
                                                        number=test_size)
    for tx_messages in tx_messages_gen:
        tx_messages_bit = messages.unpack_to_bits(tx_messages, info_length)
        tx_codewords = encoder.encode_messages(tx_messages_bit)
        tx_modulated = modulator.modulate_symbols(tx_codewords)
        for snr in test_snr:
            channel.snr_db = snr
            rx_modulated = channel.transmit_data(tx_modulated)
            for name, decoder in _decoders.items():
                rx_messages_bit = decoder.decode_messages(rx_modulated, channel)
                if 'ber' in metric:
                    errors[name][snr]['ber'] += np.count_nonzero(tx_messages_bit != rx_messages_bit)/info_length
                if 'bler' in metric:
                    rx_messages = messages.pack_to_dec(rx_messages_bit)
                    errors[name][snr]['bler'] += np.count_nonzero(np.ravel(tx_messages) != np.ravel(rx_messages))
    results = {n: {s: {k: e/test_size for k, e in vv.items()}
               for s, vv in v.items()}
               for n, v in errors.items()}
    return results


def _save_results_to_file(results, **kwargs):
    timestamp = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    filename = "{}-Results.dat".format(timestamp)
    with open(filename, 'w') as outfile:
        outfile.write("{}\n".format(kwargs))
        for name, errors in results.items():
            outfile.write("{}\n".format(name))
            outfile.write("{}\n".format(errors))


def _plot_results(results, metric='ber'):
    plt.figure()
    for name, errors in results.items():
        _values = [k[metric] for k in errors.values()]
        _snrs = errors.keys()
        plt.semilogy(_snrs, _values, label=name)
    plt.legend()
    plt.xlabel("SNR/dB")
    plt.ylabel(metric.upper())


def main():
    options = {'code_length': 16, 'snr_bob': 0., 'snr_eve': -5.}
    nn_options = {'layer': [512], 'train_snr': 5.}
    svm_options = {'C': 1., 'kernel': "rbf", 'gamma': 1}
    results = ber_vs_snr_curve(**options, nn_options=nn_options,
                               svm_options=svm_options, test_size=1e6,
                               test_snr=range(-10, 11))
    _save_results_to_file(results, **options, nn_options=nn_options,
                          svm_options=svm_options)
    pprint(results)
    _plot_results(results)
    plt.show()

if __name__ == "__main__":
    main()
