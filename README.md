# Machine learning algorithms for decoding polar wiretap codes

Machine learning algorithms used to decode polar wiretap codes. Run
`polar_wiretap_decoding.py` for simulating different decoders. The three
decoders are neural network, support vector machine and standard polar decoder.

This is the source code to the Asilomar 2018 paper "Machine Learning Assisted
Wiretapping".
